#ifndef __ANALYSIS_TABLE_CELL_H__
#define __ANALYSIS_TABLE_CELL_H__

#include <string.h>
using namespace std;

typedef struct AnalysisCell{
    string op;
    int direct;
} AnalysisCell;

#endif
