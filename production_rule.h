#ifndef __PRODUCTION_RULE_H__
#define __PRODUCTION_RULE_H__
#include <string.h>
using namespace std;

typedef struct Production{
    //int id;
    string leftPart;
    string rightPart[10];
    int rightPartLength;
} Production;

#endif
